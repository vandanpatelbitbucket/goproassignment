//
//  LandingPageViewControllerTests.swift
//  GoProAssignmentTests
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

@testable import GoProAssignment
import AVKit
import XCTest

class LandingPageViewControllerTests: XCTestCase {
    
    var viewController: LandingPageViewController?
    let presenter = LandingPagePresenterMock()

    override func setUp() {
        super.setUp()
        viewController = LandingPageViewController()
        viewController?.presenter = presenter
        _ = viewController?.view
    }

    override func tearDown() {
        viewController = nil
        super.tearDown()
    }
    
    func testDidSetPlayerAndControllerForFailure() {
        viewController?.didSetPlayerAndController(nil, nil)
        XCTAssertNil(viewController?.player)
        XCTAssertNil(viewController?.playerVC)
    }
    
    func testDidSetPlayerAndControllerForSuccess() {
        viewController?.didSetPlayerAndController(AVPlayer(playerItem: nil), AVPlayerViewController())
        XCTAssertNotNil(viewController?.playerVC)
        XCTAssertNotNil(viewController?.player)
    }
    
    func testViewDidLoad() {
        XCTAssertEqual(presenter.numberOfTimesLoadViewCalled, 1)
    }
}
