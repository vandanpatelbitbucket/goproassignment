//
//  LandingPageViewControllerMock.swift
//  GoProAssignmentTests
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

@testable import GoProAssignment
import Foundation
import AVKit

class LandingPageViewControllerMock: LandingPageViewable {
    
    var numberOfTimesDidSetPlayerAndControllerCalled = 0
    
    func didSetPlayerAndController(_ player: AVPlayer?, _ controller: AVPlayerViewController?) {
        numberOfTimesDidSetPlayerAndControllerCalled += 1
    }
}
