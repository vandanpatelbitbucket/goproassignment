//
//  LandingPagePresenterMock.swift
//  GoProAssignmentTests
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

@testable import GoProAssignment
import Foundation

class LandingPagePresenterMock: LandingPagePresentable {
    
    var numberOfTimesLoadViewCalled = 0
    
    func loadView(forResource resource: String, ofType type: String) {
        numberOfTimesLoadViewCalled += 1
    }
}
