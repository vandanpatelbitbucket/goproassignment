//
//  LandingPagePresenterTests.swift
//  GoProAssignmentTests
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

@testable import GoProAssignment
import AVKit
import XCTest

class LandingPagePresenterTests: XCTestCase {
    
    let view = LandingPageViewControllerMock()
    let playerController = AVPlayerViewController()
    var presenter: LandingPagePresenter?

    override func setUp() {
        super.setUp()
        presenter = LandingPagePresenter(view: view, playerController: playerController)
    }

    override func tearDown() {
        presenter = nil
        super.tearDown()
    }
    
    func testInitialization() {
        XCTAssertNotNil(presenter?.view)
        XCTAssert(presenter?.view is LandingPageViewControllerMock)
        XCTAssertNotNil(presenter?.playerController)
    }
    
    func testLoadViewForSuccess() {
        presenter?.loadView(forResource: "SampleVideo", ofType: "mp4")
        XCTAssertNotNil(presenter?.player)
        XCTAssertNotNil(presenter?.playerController.player)
        XCTAssertEqual(view.numberOfTimesDidSetPlayerAndControllerCalled, 1)
    }
    
    func testLoadViewForFailure() {
        presenter?.loadView(forResource: "", ofType: "")
        XCTAssertNil(presenter?.player)
        XCTAssertNil(presenter?.playerController.player)
        XCTAssertEqual(view.numberOfTimesDidSetPlayerAndControllerCalled, 1)
    }
}
