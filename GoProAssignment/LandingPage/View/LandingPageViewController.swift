//  LandingPageViewController.swift
//
//  GoProAssignment
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

import UIKit
import AVKit

protocol LandingPageViewable {
    func didSetPlayerAndController(_ controller: AVPlayerViewController?)
}

class LandingPageViewController: UIViewController, Storyboarded, LandingPageViewable {
    
    var playerVC: AVPlayerViewController?
    var presenter: LandingPagePresentable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.loadView(forResource: "SampleVideo", ofType: "mp4")
    }
    
    func didSetPlayerAndController(_ playerVC: AVPlayerViewController?) {
        self.playerVC = playerVC
    }
    
    @IBAction func didTapPlayVideo(_ sender: UIButton) {
        guard let playerVC = playerVC else { return }
        present(playerVC, animated: true) { [unowned self] in
            self.playerVC?.player?.play()
        }
    }
}
