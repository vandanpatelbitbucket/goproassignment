//
//  LandingPagePresenter.swift
//  GoProAssignment
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

import Foundation
import AVKit

protocol LandingPagePresentable {
    func loadView(forResource resource: String, ofType type: String)
}

class LandingPagePresenter: LandingPagePresentable {
    
    let view: LandingPageViewable
    let playerController: AVPlayerViewController
    var player: AVPlayer?
    
    init(view: LandingPageViewable, playerController: AVPlayerViewController) {
        self.view = view
        self.playerController = playerController
    }
    
    func loadView(forResource resource: String, ofType type: String) {
        guard
            let videoPath = Bundle.main.path(forResource: resource, ofType: type) else {
                view.didSetPlayerAndController(nil)
                return
        }
        let url = URL(fileURLWithPath: videoPath)
        player = AVPlayer(url: url)
        playerController.player = player
        view.didSetPlayerAndController(playerController)
    }
}
