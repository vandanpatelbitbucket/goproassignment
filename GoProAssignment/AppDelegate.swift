//
//  AppDelegate.swift
//  GoProAssignment
//
//  Created by Vandan Patel on 6/1/19.
//  Copyright © 2019 Vandan Patel. All rights reserved.
//

import UIKit
import AVKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let landingPageVC = LandingPageViewController.instantiate()
        let landingPagePresenter = LandingPagePresenter(view: landingPageVC, playerController: AVPlayerViewController())
        landingPageVC.presenter = landingPagePresenter
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = landingPageVC
        window?.makeKeyAndVisible()
        return true
    }
}

